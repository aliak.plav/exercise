# Attack surface service - exercise

Generate a backend and frontend stack using Python, including interactive API documentation.

## Overview

### Exercise
It was not always possible to use the best approaches and practices.
Better to split the service and use the stats endpoint separately.
I was limited in time and in a hurry.
Sorry.

### Test data
Use ./examples/cloud.json

### Run service

Execute command:
```bash
docker-compose up --build
```

* Main service  http://localhost:8080/swagger
* JaegerUI http://localhost:16686/search

### API's
* /api/v1/attack - to fetch list of accessible machines
* /api/v1/stats - to get information about timings and vms
* /api/maintenance - bonus section for upload json data

### Usage
* To fetch list of accessible machines for vm-0000001 use:
```bash
curl http://localhost:8080/api/v1/attack?vm_id=vm-0000001
```
  PostMan: http://localhost:8080/api/v1/attack?vm_id=vm-0000001

* To get request stats use:
```bash
curl http://localhost:8080/api/v1/stats/
```
PostMan: http://localhost:8080/api/v1/stats/


## What's inside
* FastAPI project
* Jaeger (query, collector, agent, ui)
* ElasticSearch
* redis

## Performance
### Performance features
* lru_cache. It's simple and enough for this project. Variants of a redis-based cache were also considered 
* redis. Settins: AOF + fsync everysec. Optimal in performance and data persistence.
* Indexing data. Input data is indexed and stored in radis. This approach reduces the load on the service and significantly increases performance with further requests. This is optimal for such volumes of data

### Performance features. Action items.
* Can be increased by server settings, load balancer, different architecture.
* Jeager + elasticserach with default settings is not the best approach in tracing.
* Different approaches to caching can be used.

## Tracing
* OpenTracing approach is used. It allows you to see the work of the service in detail. It is scalable.
* Follow to http://localhost:16686/search to get service statistics in detail
* No profiling, custom algorithms and noname vendors packages.

## Security
### Security. Action items.
* No authentication/authorization process.
* Ports rules must be changed.
* Policies: CORS sources, allowed credentials, allowed methods must be checked and updated.
* Using public docker images and packages. It is recommended to use containers from the corporate hub.

### Security. What was done
* All versions of packages have been locked. (Except Python ver due public docker image)
* Input data validators.
* Using only official packages or officially recommended packages whenever possible

## CI/CD
* Pre-comming configuration. Just linter running.
* gitlab CI. Only tests running.

## Unit tests
Only several unit tests. Two endpoints covered. Not 100% coverage :) Sorry


## Logs
I didn't use. 
It I need to build a separate service for collecting logs. It can be fluentd with ElasticSearch or something like this.



from typing import Generator

from app.clients import jaeger_client, redis_client


def get_redis() -> Generator:
    yield redis_client


def get_jaeger() -> Generator:
    yield jaeger_client

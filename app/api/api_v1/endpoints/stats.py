from typing import Any

from fastapi import APIRouter, Depends

from app.api import deps
from app.clients.jaeger import JaegerClient
from app.clients.redis import RedisClient
from app.schemas.stat import ResponseStats
from app.service.timing_stats import TimingStatsService
from app.service.vm_stats import VmStatsService

router = APIRouter()


@router.get("/", response_model=ResponseStats)
def get_attack_stats(
    redis: RedisClient = Depends(deps.get_redis),
    jaeger_client: JaegerClient = Depends(deps.get_jaeger)
) -> Any:
    check_endpoint = "attack"

    vm_count = VmStatsService(redis).get_vms_count()
    timing_data = TimingStatsService(jaeger_client).get_endpoint_stats(check_endpoint)

    return ResponseStats(
        vm_count=vm_count,
        request_count=timing_data.request_count,
        average_request_time=timing_data.average_request_time
    )

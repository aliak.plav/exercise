from functools import lru_cache
from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException, status

from app.api import deps
from app.clients.redis import RedisClient
from app.schemas.machine import Machine
from app.service.cloud_structure import CloudStructureService
from app.service.vm_stats import VmStatsService

router = APIRouter()


@router.get("/", response_model=List[str])
@lru_cache
def read_attack(vm_id: str, redis: RedisClient = Depends(deps.get_redis)) -> Any:
    try:
        Machine.check_vm_id_format(vm_id)
        cloud_vm = CloudStructureService(redis).get(vm_id)
        attack = cloud_vm.accessible_machines
    except ValueError as e:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=str(e)
        )
    except KeyError:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Virtual machine does not exists in the system"
        )

    VmStatsService(redis).add_vm(vm_id)
    return attack

from fastapi import APIRouter

from app.api.api_v1.endpoints import attack, stats

router = APIRouter()
router.include_router(attack.router, prefix="/attack", tags=["Attack endpoints"])
router.include_router(stats.router, prefix="/stats", tags=["Statistics"])

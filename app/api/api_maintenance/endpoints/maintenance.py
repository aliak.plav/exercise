from typing import Any, List

from fastapi import APIRouter, Depends

from app.api import deps
from app.clients.redis import RedisClient
from app.core.cache import clear_cache
from app.schemas.structure import CloudStructure, FlatCloudStructure
from app.service.cloud_structure import CloudStructureService

router = APIRouter()


@router.post("/update", response_model=List[FlatCloudStructure])
def read_items(
    structure: CloudStructure,
    redis: RedisClient = Depends(deps.get_redis)
) -> Any:
    clear_cache()
    return CloudStructureService(redis).load_structure(structure)

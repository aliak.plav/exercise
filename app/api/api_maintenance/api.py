from fastapi import APIRouter

from app.api.api_maintenance.endpoints import maintenance

router = APIRouter()
router.include_router(maintenance.router, prefix="/maintenance", tags=["Maintenance"])

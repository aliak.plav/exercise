from typing import List

from pydantic import BaseModel

from .machine import Machine
from .rule import Rule


class CloudStructure(BaseModel):
    vms: List[Machine]
    fw_fules: List[Rule]


class FlatCloudStructure(BaseModel):
    machine_id: str = ''
    accessible_machines: List[str] = []

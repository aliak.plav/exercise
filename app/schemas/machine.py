import re
from typing import List

from pydantic import BaseModel, validator


class Machine(BaseModel):
    vm_id: str
    name: str
    tags: List[str]

    @validator('vm_id')
    def check_vm_id_format(cls, v):
        if not re.match(r'vm-[a-z0-9]{7}$', v, re.IGNORECASE):
            raise ValueError(
                f"Incorrect vm_id format. {v} does not match vm-xxxxxxx")
        return v

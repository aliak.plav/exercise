from pydantic import BaseModel


class Stat(BaseModel):
    count: int = 0


class TimingStats(BaseModel):
    request_count: int = 0
    average_request_time: float = 0


class ResponseStats(TimingStats):
    vm_count: int = 0

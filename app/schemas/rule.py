import re

from pydantic import BaseModel, validator


class Rule(BaseModel):
    fw_id: str
    source_tag: str
    dest_tag: str

    @validator('fw_id')
    def check_vm_id_format(cls, v):
        if not re.match(r'fw-[a-z0-9]{5}$', v, re.IGNORECASE):
            raise ValueError(f'Incorrect fw_id format. {v} does not match fw-xxxxx')
        return v

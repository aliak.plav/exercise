from app.clients.redis import RedisClient
from app.core.config import settings
from app.schemas.stat import Stat

from .redis import RedisService


class VmStatsService(RedisService[Stat]):
    def __init__(self, redis: RedisClient, group: str = settings.REDIS_STATS_STORAGE_KEY):
        self._group = group
        super().__init__(redis, Stat)

    def add_vm(self, vm_id: str):
        try:
            stat = self.get(vm_id)
        except Exception:
            stat = Stat()
        finally:
            stat.count += 1
            self.set(vm_id, stat)

    def get_vms_count(self):
        return self.hlen()

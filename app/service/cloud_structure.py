import json
from typing import List

from app.clients.redis import RedisClient
from app.core.config import settings
from app.schemas.structure import CloudStructure, FlatCloudStructure

from .redis import RedisService


class CloudStructureService(RedisService[FlatCloudStructure]):
    def __init__(self, redis: RedisClient, group: str = settings.REDIS_VMS_STORAGE_KEY):
        self._group = group
        super().__init__(redis, FlatCloudStructure)

    def load_structure(self, structure: CloudStructure) -> List[FlatCloudStructure]:
        self.clear()

        flattened_structure = self._flatten_structure(structure)
        values = dict((obj.machine_id, json.dumps(obj.dict()))
                      for obj in flattened_structure)
        self.hmset(values)

        return flattened_structure

    def _flatten_structure(self, structure: CloudStructure) -> List[FlatCloudStructure]:
        vm_ids = {}
        tags = {}
        for vm in structure.vms:
            vm_id = vm.vm_id
            vm_ids[vm_id] = []

            for tag in vm.tags:
                if tag in tags:
                    tags[tag].append(vm_id)
                else:
                    tags[tag] = [vm_id]

        for rule in structure.fw_fules:
            if rule.source_tag in tags and rule.dest_tag in tags:

                for source_vm_id in tags[rule.source_tag]:
                    vm_ids_dest = vm_ids[source_vm_id]

                    for dest_vm_id in tags[rule.dest_tag]:
                        if source_vm_id != dest_vm_id and dest_vm_id not in vm_ids_dest:
                            vm_ids_dest.append(dest_vm_id)

        return [FlatCloudStructure(machine_id=key, accessible_machines=data) for key, data in vm_ids.items()]

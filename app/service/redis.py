import json
from typing import Generic, Type, TypeVar

from pydantic import BaseModel

from app.clients.redis import RedisClient

SchemaType = TypeVar("SchemaType", bound=BaseModel)


class RedisService(Generic[SchemaType]):
    def __init__(self, redis: RedisClient, model: Type[SchemaType]):
        self._redis = redis
        self._model = model

    def get(self, key: str) -> SchemaType:
        value = self._redis.get(self._group, key)
        if value is None:
            raise KeyError("Key not found")
        return self._model.parse_obj(json.loads(value))

    def set(self, key: str, obj: SchemaType):
        value = json.dumps(obj.dict())
        self._redis.set(self._group, key, value)

    def hmset(self, values: dict):
        self._redis.hmset(self._group, values)

    def hlen(self) -> int:
        return self._redis.hlen(self._group)

    def clear(self, key: str = None):
        self._redis.clear(self._group, key)

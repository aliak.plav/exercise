from app.clients.jaeger import JaegerClient
from app.core.config import settings
from app.schemas.stat import TimingStats


class TimingStatsService(object):
    def __init__(
        self, jaeger_client: JaegerClient, service: str = settings.PROJECT_NAME, prefix: str = settings.ENDPOINT_STATS
    ):
        self._jaeger_client = jaeger_client
        self._service = service
        self._prefix = prefix

    def get_endpoint_stats(self, endpoint: str) -> TimingStats:
        operation = self._get_operation(endpoint)
        return self._get_operation_stats(self._service, operation)

    def _get_operation(self, endpoint: str) -> str:
        return f"{self._prefix}{endpoint}/"

    def _get_operation_stats(self, service: str, operation: str) -> TimingStats:
        jaeger_data = self._jaeger_client.get_traces(service, operation)
        json_data = jaeger_data.json()
        return self._get_timing_stats_from_response(json_data)

    def _get_timing_stats_from_response(self, response_data: dict) -> TimingStats:
        total_ms = sum([data["spans"][0]["duration"]
                       for data in response_data["data"]])
        count = len(response_data["data"])

        average_us = count and total_ms/count
        average_ms = average_us/1000
        return TimingStats(request_count=count, average_request_time=average_ms)

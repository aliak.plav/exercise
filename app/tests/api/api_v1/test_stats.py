from fastapi.testclient import TestClient

from app.core.config import settings


def test_stats(
    client: TestClient()
) -> None:
    response = client.get(f"{settings.API_V1_STR}/stats/")
    assert response.status_code == 200

    content = response.json()
    assert "vm_count" in content
    assert "request_count" in content
    assert "average_request_time" in content

    request_count = content["content"]
    average_request_time = content["average_request_time"]
    assert bool(request_count) == bool(average_request_time)

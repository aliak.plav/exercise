from fastapi.testclient import TestClient

from app.core.config import settings


def test_input_validation(
    client: TestClient()
) -> None:
    params = {"vm_id": "vvvm-0123456"}
    response = client.get(f"{settings.API_V1_STR}/attack/", params=params)
    assert response.status_code == 422


def test_not_found(
    client: TestClient()
) -> None:
    params = {"vm_id": "vm-0123456"}
    response = client.get(f"{settings.API_V1_STR}/attack/", params=params)
    assert response.status_code == 404


def test_success(
    client: TestClient()
) -> None:
    params = {"vm_id": "vm-000000"}
    response = client.get(f"{settings.API_V1_STR}/attack/", params=params)
    assert response.status_code == 200

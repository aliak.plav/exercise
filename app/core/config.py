from typing import List, Union

from pydantic import AnyHttpUrl, BaseSettings, validator


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    API_MAINTENANCE_STR: str = '/api/maintenance'
    REDIS_STATS_STORAGE_KEY: str
    REDIS_VMS_STORAGE_KEY: str

    PROJECT_NAME: str
    PROJECT_VERSION: str
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    JAEGER_AGENT_HOST: str
    JAEGER_AGENT_PORT: int
    JAEGER_QUERY_HOST: str
    JAEGER_QUERY_PORT: int

    REDIS_HOST: str
    REDIS_PORT: int

    ENDPOINT_STATS: str


settings = Settings()

from jaeger_client import Config, Tracer
from opentracing.scope_managers.contextvars import ContextVarsScopeManager


def init_tracer(service_name: str, host: str, port: int) -> Tracer:
    config = Config(
        config={
            "local_agent": {
                "reporting_host": host,
                "reporting_port": port,
            },
            "sampler": {
                "type": 'probabilistic',
                "param": 1.0
            },
            "trace_id_header": 'x-trace-id',
        },
        scope_manager=ContextVarsScopeManager(),
        service_name=service_name,
        validate=True,
    )
    return config.initialize_tracer()

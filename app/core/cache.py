import functools
import gc


def clear_cache():
    gc.collect()

    objects = [i for i in gc.get_objects()
               if isinstance(i, functools._lru_cache_wrapper)]

    for object in objects:
        object.cache_clear()

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette_opentracing import StarletteTracingMiddleWare

from app.api.api_maintenance.api import router as api_maintenance_router
from app.api.api_v1.api import router as api_v1_router
from app.core.config import settings
from app.core.tracer import init_tracer

app = FastAPI(
    title=settings.PROJECT_NAME,
    docs_url='/swagger',
    version=settings.PROJECT_VERSION
)

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin)
                       for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_v1_router, prefix=settings.API_V1_STR)
app.include_router(api_maintenance_router, prefix=settings.API_MAINTENANCE_STR)


@app.on_event('startup')
async def startup():
    app.state.tracer = init_tracer(
        settings.PROJECT_NAME, settings.JAEGER_AGENT_HOST, settings.JAEGER_AGENT_PORT)
    app.add_middleware(StarletteTracingMiddleWare, tracer=app.state.tracer)

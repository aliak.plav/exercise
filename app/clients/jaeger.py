from requests import get

from app.core.config import settings


class JaegerClient(object):
    def __init__(self, host: str, port: int, protocol: str = 'http'):
        self._host = host
        self._port = port
        self._protocol = protocol

    def get_traces(self, service: str, operation: str):
        params = {"service": service, "operation": operation}
        return self._get_data("/api/traces", params)

    def _get_data(self, endpoint: str, params: dict):
        url = f"{self._protocol}://{self._host}:{self._port}{endpoint}"
        return get(url, params)


jaeger_client = JaegerClient(settings.JAEGER_QUERY_HOST, settings.JAEGER_QUERY_PORT)

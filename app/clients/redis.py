import redis

from app.core.config import settings


class RedisClient(object):
    def __init__(self, host: str, port: int):
        self._client = redis.Redis(host=host, port=port)

    def get(self, group_name: str, key: str) -> str:
        return self._client.hget(group_name, key)

    def set(self, group_name: str, key: str, value: str):
        self._client.hset(group_name, key, value)

    def hmset(self, group_name: str, values: str):
        self._client.hmset(group_name, values)

    def hlen(self, group_name: str) -> int:
        return self._client.hlen(group_name)

    def hdel(self, group_name: str, key: str):
        self._client.hdel(group_name, key)

    def hdelall(self, group_name: str):
        all_keys = list(self._client.hgetall(group_name).keys())
        if all_keys:
            self._client.hdel(group_name, *all_keys)

    def clear(self, group_name: str, key: str = None):
        if key:
            self.hdel(group_name, key)
        else:
            self.hdelall(group_name)


redis_client = RedisClient(settings.REDIS_HOST, settings.REDIS_PORT)
